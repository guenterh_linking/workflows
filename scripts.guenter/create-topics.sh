#!/usr/bin/env bash

echo "create all import topics"

kafka-topics.sh --create --topic wikidata-all --bootstrap-server 192.168.1.53:9092 --partitions 3
kafka-topics.sh --create --topic wikidata-filtered --bootstrap-server 192.168.1.53:9092 --partitions 6
kafka-topics.sh --create --topic wikidata-categorized --bootstrap-server 192.168.1.53:9092 --partitions 6
kafka-topics.sh --create --topic gnd-all --bootstrap-server 192.168.1.53:9092 --partitions 3
kafka-topics.sh --create --topic gnd-categorized --bootstrap-server 192.168.1.53:9092 --partitions 3

