#!/usr/bin/env bash

echo "start wikidata file import"
cd /home/swissbib/environment/code/repositories/guenterh/guenterh_linking/kafka-producer-files/k8s-manifests/test
kubectl apply -f kafka-producer-files-wikidata-test-deployment.yaml

echo "start gnd file import"
kubectl apply -f kafka-producer-files-gnd-test-job.yaml

echo "start wikidata filter"
cd /home/swissbib/environment/code/repositories/guenterh/guenterh_linking/wikidata-import-filter/k8s-manifests/test
kubectl apply -f wikidata-import-filter-test-deployment.yaml

echo "start create wikidata entities"
cd /home/swissbib/environment/code/repositories/guenterh/guenterh_linking/kafka-streams-wikidata-entities/k8s-manifests/test
kubectl apply -f wikidata-entities-test-deployment.yaml

echo "start create gnd entities"
cd /home/swissbib/environment/code/repositories/guenterh/guenterh_linking/kafka-streams-gnd-import/k8s-manifests/test
kubectl apply -f gnd-entities-test-deployment.yaml

