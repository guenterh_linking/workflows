#!/usr/bin/env bash

echo "delete all import topics"

kafka-topics.sh --delete --topic wikidata-all --bootstrap-server 192.168.1.53:9092
kafka-topics.sh --delete --topic wikidata-filtered --bootstrap-server 192.168.1.53:9092
kafka-topics.sh --delete --topic wikidata-categorized --bootstrap-server 192.168.1.53:9092
kafka-topics.sh --delete --topic gnd-all --bootstrap-server 192.168.1.53:9092
kafka-topics.sh --delete --topic gnd-categorized --bootstrap-server 192.168.1.53:9092

