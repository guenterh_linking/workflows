#!/usr/bin/env bash

echo "delete all id-hub topics"

kafka-topics.sh --delete --topic id-hub-organisations-source --bootstrap-server 192.168.1.53:9092
echo
kafka-topics.sh --delete --topic id-hub-organisations-sink --bootstrap-server 192.168.1.53:9092
echo
kafka-topics.sh --delete --topic id-hub-persons-sink --bootstrap-server 192.168.1.53:9092
echo
kafka-topics.sh --delete --topic id-hub-persons-source --bootstrap-server 192.168.1.53:9092
echo
