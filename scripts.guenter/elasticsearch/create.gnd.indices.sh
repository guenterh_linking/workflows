#!/usr/bin/env bash

DAY=2022-05-22
HOST=192.168.1.53
PORT=9200


#label mapping
curl -XPUT -H "Content-Type: application/json" --data "@./gnd.mappings/mapping.label.json" \
  "http://$HOST:$PORT/gnd-undiff-persons-$DAY"
echo
curl -XPUT -H "Content-Type: application/json" --data "@./gnd.mappings/mapping.label.json" \
  "http://$HOST:$PORT/gnd-works-$DAY"
echo
curl -XPUT -H "Content-Type: application/json" --data "@./gnd.mappings/mapping.label.json" \
"http://$HOST:$PORT/gnd-places-$DAY"
echo

#subject mapping
curl -XPUT -H "Content-Type: application/json" --data "@./gnd.mappings/mapping.subject.json" \
"http://$HOST:$PORT/gnd-subjects-$DAY"
echo

#mapping person
curl -XPUT -H "Content-Type: application/json" --data "@./gnd.mappings/mapping.person.json" \
"http://$HOST:$PORT/gnd-persons-$DAY"
echo

#mapping organisation
curl -XPUT -H "Content-Type: application/json" --data "@./gnd.mappings/mapping.organisation.json" \
"http://$HOST:$PORT/gnd-organisations-$DAY"
echo
curl -XPUT -H "Content-Type: application/json" --data "@./gnd.mappings/mapping.organisation.json" \
"http://$HOST:$PORT/gnd-conferences-$DAY"
echo