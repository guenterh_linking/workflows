#!/usr/bin/env bash

DAY=2022-05-20
HOST=192.168.1.53
PORT=9200


#persons mapping
curl -XPUT -H "Content-Type: application/json" --data "@./wikidata.mappings/mapping.persons.json" \
  "http://$HOST:$PORT/wikidata-persons-$DAY"
echo

#organization mapping
curl -XPUT -H "Content-Type: application/json" --data "@./wikidata.mappings/mapping.organisations.json" \
"http://$HOST:$PORT/wikidata-organisations-$DAY"
echo

#items mapping
curl -XPUT -H "Content-Type: application/json" --data "@./wikidata.mappings/mapping.items.json" \
"http://$HOST:$PORT/wikidata-items-$DAY"
echo

#mapping events
curl -XPUT -H "Content-Type: application/json" --data "@./wikidata.mappings/mapping.events.json" \
"http://$HOST:$PORT/wikidata-events-$DAY"
echo

