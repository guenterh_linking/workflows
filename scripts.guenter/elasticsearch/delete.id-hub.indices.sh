#!/usr/bin/env bash

DAY=2022-05-22
HOST=192.168.1.53
PORT=9200


#label mapping
curl -XDELETE -H "Content-Type: application/json" "http://$HOST:$PORT/id-hub-persons-$DAY"
echo
curl -XDELETE -H "Content-Type: application/json" "http://$HOST:$PORT/id-hub-organisations-$DAY"
echo

