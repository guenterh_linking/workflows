#!/usr/bin/env bash

DAY=2022-05-22
HOST=192.168.1.53
PORT=9200


#label mapping
curl -XDELETE -H "Content-Type: application/json" "http://$HOST:$PORT/gnd-undiff-persons-$DAY"
echo
curl -XDELETE -H "Content-Type: application/json" "http://$HOST:$PORT/gnd-works-$DAY"
echo
curl -XDELETE -H "Content-Type: application/json" "http://$HOST:$PORT/gnd-places-$DAY"
echo
curl -XDELETE -H "Content-Type: application/json" "http://$HOST:$PORT/gnd-subjects-$DAY"
echo
curl -XDELETE -H "Content-Type: application/json" "http://$HOST:$PORT/gnd-persons-$DAY"
echo
curl -XDELETE -H "Content-Type: application/json" "http://$HOST:$PORT/gnd-organisations-$DAY"
echo
curl -XDELETE -H "Content-Type: application/json" "http://$HOST:$PORT/gnd-conferences-$DAY"
echo


