#!/usr/bin/env bash

echo "create all id-hub topics"

kafka-topics.sh --create --topic id-hub-organisations-source --bootstrap-server 192.168.1.53:9092 --partitions 3
kafka-topics.sh --create --topic id-hub-organisations-sink --bootstrap-server 192.168.1.53:9092 --partitions 3
kafka-topics.sh --create --topic id-hub-persons-sink --bootstrap-server 192.168.1.53:9092 --partitions 3
kafka-topics.sh --create --topic id-hub-persons-source --bootstrap-server 192.168.1.53:9092 --partitions 3