#!/usr/bin/env bash

echo "delete gnd Kategorien"
kubectl delete deployments.apps linking-gnd-entities-test-import

echo "delete import gnd lobid"
kubectl delete jobs.batch linking-kafka-producer-gnd-files-import-test

echo "delete import wikidata"
kubectl delete jobs.batch linking-kafka-producer-wikidata-files-import-test

echo "delete wikidata Kategorien"
kubectl delete deployments.apps linking-wikidata-entities-test-import

echo "delete import wikidata filter"
kubectl delete deployments.apps linking-wikidata-import-filter-import

echo "delete consumer elasticsearch - read GND / wikidata Kategorien"
kubectl delete deployments.apps linking-consumer-elasticsearch-test