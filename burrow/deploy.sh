#!/usr/bin/env bash

docker build -t swissbib/burrow:latest .
docker login
docker push swissbib/burrow:latest


docker stack deploy --compose-file docker-compose.yml kafka-monitoring