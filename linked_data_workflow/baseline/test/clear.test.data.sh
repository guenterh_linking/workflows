#!/usr/bin/env bash

source ../../env.sh
DUMP_DATE=${NEW_INDEX_DATE}
curl -XDELETE "http://sb-ues5.swissbib.unibas.ch:8080/test-sb-resources-${DUMP_DATE}"
curl -XDELETE "http://sb-ues5.swissbib.unibas.ch:8080/test-sb-items-${DUMP_DATE}"
curl -XDELETE "http://sb-ues5.swissbib.unibas.ch:8080/test-sb-documents-${DUMP_DATE}"
curl -XDELETE "http://sb-ues5.swissbib.unibas.ch:8080/test-sb-persons-${DUMP_DATE}"
curl -XDELETE "http://sb-ues5.swissbib.unibas.ch:8080/test-sb-organisations-${DUMP_DATE}"