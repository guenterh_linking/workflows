#!/usr/bin/env bash

source ../../env.sh

DUMP_DATE=${NEW_INDEX_DATE}
export SWISSBIB_TEST_CONFIGS=/swissbib_index/nas/configurations/swissbib/baseline/test

export ELASTIC_SEARCH_INDEX=id-hub
export ELASTIC_RERO_INDEX=rero-viaf-links

mkdir -p ${SWISSBIB_TEST_CONFIGS}

sed "s/__DUMP_DATE__/$DUMP_DATE/g" ./configs/config.document.properties > "${SWISSBIB_TEST_CONFIGS}/config.document.properties"
sed "s/__DUMP_DATE__/$DUMP_DATE/g" ./configs/config.resource.properties > "${SWISSBIB_TEST_CONFIGS}/config.resource.properties"
sed "s/__DUMP_DATE__/$DUMP_DATE/g" ./configs/config.item.properties > "${SWISSBIB_TEST_CONFIGS}/config.item.properties"
sed "s/__DUMP_DATE__/$DUMP_DATE/g" ./configs/config.person.properties > "${SWISSBIB_TEST_CONFIGS}/config.person.properties"
sed "s/__DUMP_DATE__/$DUMP_DATE/g" ./configs/config.organisation.properties > "${SWISSBIB_TEST_CONFIGS}/config.organisation.properties"

curl -XPUT -H "Content-Type: application/json" --data "@./mappings/organisations.json" "http://sb-ues5.swissbib.unibas.ch:8080/test-sb-organisations-${DUMP_DATE}"
echo "\n"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/documents.json" "http://sb-ues5.swissbib.unibas.ch:8080/test-sb-documents-${DUMP_DATE}"
echo "\n"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/items.json" "http://sb-ues5.swissbib.unibas.ch:8080/test-sb-items-${DUMP_DATE}"
echo "\n"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/persons.json" "http://sb-ues5.swissbib.unibas.ch:8080/test-sb-persons-${DUMP_DATE}"
echo "\n"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/resources.json" "http://sb-ues5.swissbib.unibas.ch:8080/test-sb-resources-${DUMP_DATE}"
echo "\n"
mkdir -p ${SWISSBIB_TEST_CONFIGS}/consumer && cp ./configs/app.consumer.properties "$_/app.properties"

kafka-topics-test create sb-categorized
kafka-topics-test create sb-resources-sink
kafka-topics-test create sb-linker-source

docker stack deploy -c docker-compose.yml test-swissbib-baseline