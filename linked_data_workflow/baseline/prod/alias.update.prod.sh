#!/usr/bin/env bash


source ../../env.sh



curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"sb-persons-${NEW_INDEX_DATE}\", \"alias\" : \"sb-persons\" } },\
{ \"add\" : { \"index\" : \"sb-organisations-${NEW_INDEX_DATE}\", \"alias\" : \"sb-organisations\" } },\
{ \"add\" : { \"index\" : \"sb-items-${NEW_INDEX_DATE}\", \"alias\" : \"sb-items\" } },\
{ \"add\" : { \"index\" : \"sb-documents-${NEW_INDEX_DATE}\", \"alias\" : \"sb-documents\" } },\
{ \"add\" : { \"index\" : \"sb-resources-${NEW_INDEX_DATE}\", \"alias\" : \"sb-resources\" } },\
{ \"remove\" : { \"index\" : \"sb-persons-${OLD_INDEX_DATE}\", \"alias\" : \"sb-persons\" } },\
{ \"remove\" : { \"index\" : \"sb-organisations-${OLD_INDEX_DATE}\", \"alias\" : \"sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"sb-items-${OLD_INDEX_DATE}\", \"alias\" : \"sb-items\" } },\
{ \"remove\" : { \"index\" : \"sb-documents-${OLD_INDEX_DATE}\", \"alias\" : \"sb-documents\" } },\
{ \"remove\" : { \"index\" : \"sb-resources-${OLD_INDEX_DATE}\", \"alias\" : \"sb-resources\" } }\
]\
}"