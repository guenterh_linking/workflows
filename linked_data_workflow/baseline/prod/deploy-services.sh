#!/usr/bin/env bash

source ../../env.sh

DUMP_DATE=${NEW_INDEX_DATE}
SWISSBIB_CONFIGS=/swissbib_index/nas/configurations/swissbib/baseline

export ELASTIC_SEARCH_INDEX=id-hub
export ELASTIC_RERO_INDEX=rero-viaf-links

mkdir -p ${SWISSBIB_CONFIGS}

sed "s/__DUMP_DATE__/$DUMP_DATE/g" ./configs/config.document.properties > "${SWISSBIB_CONFIGS}/config.document.properties"
sed "s/__DUMP_DATE__/$DUMP_DATE/g" ./configs/config.resource.properties > "${SWISSBIB_CONFIGS}/config.resource.properties"
sed "s/__DUMP_DATE__/$DUMP_DATE/g" ./configs/config.item.properties > "${SWISSBIB_CONFIGS}/config.item.properties"
sed "s/__DUMP_DATE__/$DUMP_DATE/g" ./configs/config.person.properties > "${SWISSBIB_CONFIGS}/config.person.properties"
sed "s/__DUMP_DATE__/$DUMP_DATE/g" ./configs/config.organisation.properties > "${SWISSBIB_CONFIGS}/config.organisation.properties"

curl -XPUT -H "Content-Type: application/json" --data "@./mappings/organisations.json" "http://sb-ues5.swissbib.unibas.ch:8080/sb-organisations-${DUMP_DATE}"
echo "\n"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/documents.json" "http://sb-ues5.swissbib.unibas.ch:8080/sb-documents-${DUMP_DATE}"
echo "\n"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/items.json" "http://sb-ues5.swissbib.unibas.ch:8080/sb-items-${DUMP_DATE}"
echo "\n"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/persons.json" "http://sb-ues5.swissbib.unibas.ch:8080/sb-persons-${DUMP_DATE}"
echo "\n"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/resources.json" "http://sb-ues5.swissbib.unibas.ch:8080/sb-resources-${DUMP_DATE}"
echo "\n"
mkdir -p /swissbib_index/nas/configurations/swissbib/consumer && cp ./configs/app.consumer.properties "$_/app.properties"

kafka-topics create sb-categorized
kafka-topics create sb-resources-sink
kafka-topics create sb-linker-source

docker stack deploy -c docker-compose.yml swissbib-baseline