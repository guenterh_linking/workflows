#!/usr/bin/env bash

kafka-topics-test create sb-enricher-source
kafka-topics-test create sb-enricher-sink

docker stack deploy -c docker-compose.yml test-swissbib-enricher