#!/usr/bin/env bash

kafka-topics create sb-enricher-source
kafka-topics create sb-enricher-sink

docker stack deploy -c docker-compose.yml swissbib-enricher