#!/usr/bin/env bash

export ELASTIC_SEARCH_INDEX=id-hub
export ELASTIC_RERO_INDEX=rero-viaf-links

kafka-topics-test create sb-linker-source
kafka-topics-test create sb-enricher-source

docker stack deploy -c docker-compose.yml test-swissbib-linker