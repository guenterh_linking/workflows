#!/usr/bin/env bash

mkdir -p /swissbib_index/nas/configurations/flux_runner/cbs_to_kafka && cp ./flux/* "$_/"
kafka-topics create sb-all
docker-compose up -d