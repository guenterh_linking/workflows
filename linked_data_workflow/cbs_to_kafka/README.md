### CBS Kafka Connector

Ein Micro Service um die Daten eines Gesamtexportes von CBS in Kafka einzuspielen.
Dies muss nur geschehen, wenn ein neuer Gesamtexport gemacht wurde. Normalerweise
sollten diese Daten vom MessageCatcher eingespielt werden.

Bevor der Workflow gestartet wird muss die [Flux Datei](fluxbsadapter.flux) angepasst werden, damit
der neuste export geladen wird und nicht ein alter.

Welches der neuste Export ist sollte Silvia Witzig wissen.

Ansonsten auf dem Server nachschauen.

```
ssh cbs_chb@sb-rcbs4i.swissbib.unibas.ch

cd /swissbib/data/transexport/cjm/
```

In diesem Verzeichniss den letzten JOBX/RUNX auswählen. Sicherstellen, dass die Range in der Flux Datei
noch aktuell ist (sie kann bei jedem Export höher werden).

Anschliessend kann der Container mit [deploy.sh](prod/deploy.sh) gestartet werden!

Im `test/` Verzeichnis befindet sich eine Konfiguration um die Daten auf den Kafka Test Cluster zu spielen!