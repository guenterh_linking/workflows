#!/usr/bin/env bash

kafka-topics-test create sb-all-preprocessed
docker stack deploy -c docker-compose.yml test-swissbib-baseline-preprocessor