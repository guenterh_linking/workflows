#!/usr/bin/env bash

kafka-topics create sb-all-preprocessed
docker stack deploy -c docker-compose.yml swissbib-baseline-preprocessor