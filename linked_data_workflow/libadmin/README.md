## Import LibAdmin Libraries

Run `start-workflow.sh` to update the libadmin libraries. It will download the latest
mapportal.json file and then index organisations in the specified index.

**IMPORTANT**: Update the index specified in the [workflow.flux](configs/workflow.flux) file.