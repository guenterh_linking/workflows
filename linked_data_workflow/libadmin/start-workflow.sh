#!/usr/bin/env bash

source ././../env.sh

wget -O data/mapportal.json https://www.swissbib.ch/mapportal.json

mkdir -p /swissbib_index/nas/configurations/flux_runner/libadmin && cp -r ./configs "$_/"
mkdir -p /swissbib_index/nas/configurations/flux_runner/libadmin && cp -r ./data "$_/"

curl -XPUT -H "Content-Type: application/json" --data "@./configs/mapping-admin-orgs.json" "http://sb-ues5.swissbib.unibas.ch:8080/sb-organisations-admin-$NEW_INDEX_DATE"

export NEW_INDEX_DATE=${NEW_INDEX_DATE}
echo "Write organisation data into sb-organisations-admin-$NEW_INDEX_DATE."

docker-compose pull
docker-compose up -d