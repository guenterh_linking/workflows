#!/usr/bin/env bash

source ./../env.sh



curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"sb-organisations-admin-${NEW_INDEX_DATE}\", \"alias\" : \"sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"sb-organisations-admin-${OLD_INDEX_DATE}\", \"alias\" : \"sb-organisations\" } }\
]\
}"