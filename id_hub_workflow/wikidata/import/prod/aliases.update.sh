#!/usr/bin/env bash


NEW_DUMP_DATE=2020-03-08

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"wikidata-organisations-${NEW_DUMP_DATE}\", \"alias\" : \"wikidata-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"wikidata-organisations-${NEW_DUMP_DATE}\", \"alias\" : \"wikidata-sb-organisations-next\" } },\
{ \"add\" : { \"index\" : \"wikidata-organisations-${NEW_DUMP_DATE}\", \"alias\" : \"wikidata-all\" } },\
{ \"add\" : { \"index\" : \"wikidata-events-${NEW_DUMP_DATE}\", \"alias\" : \"wikidata-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"wikidata-events-${NEW_DUMP_DATE}\", \"alias\" : \"wikidata-sb-organisations-next\" } },\
{ \"add\" : { \"index\" : \"wikidata-events-${NEW_DUMP_DATE}\", \"alias\" : \"wikidata-all\" } },\
{ \"add\" : { \"index\" : \"wikidata-persons-${NEW_DUMP_DATE}\", \"alias\" : \"wikidata-persons\" } },\
{ \"remove\" : { \"index\" : \"wikidata-persons-${NEW_DUMP_DATE}\", \"alias\" : \"wikidata-persons-next\" } },\
{ \"add\" : { \"index\" : \"wikidata-persons-${NEW_DUMP_DATE}\", \"alias\" : \"wikidata-all\" } },\
{ \"add\" : { \"index\" : \"wikidata-items-${NEW_DUMP_DATE}\", \"alias\" : \"wikidata-items\" } },\
{ \"add\" : { \"index\" : \"wikidata-items-${NEW_DUMP_DATE}\", \"alias\" : \"wikidata-all\" } }\
]\
}"