#!/usr/bin/env bash


OLD_DUMP_DATE=2020-02-22

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"remove\" : { \"index\" : \"wikidata-organisations-${OLD_DUMP_DATE}\", \"alias\" : \"wikidata-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"wikidata-organisations-${OLD_DUMP_DATE}\", \"alias\" : \"wikidata-all\" } },\
{ \"remove\" : { \"index\" : \"wikidata-events-${OLD_DUMP_DATE}\", \"alias\" : \"wikidata-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"wikidata-events-${OLD_DUMP_DATE}\", \"alias\" : \"wikidata-all\" } },\
{ \"remove\" : { \"index\" : \"wikidata-persons-${OLD_DUMP_DATE}\", \"alias\" : \"wikidata-persons\" } },\
{ \"remove\" : { \"index\" : \"wikidata-persons-${OLD_DUMP_DATE}\", \"alias\" : \"wikidata-all\" } },\
{ \"remove\" : { \"index\" : \"wikidata-items-${OLD_DUMP_DATE}\", \"alias\" : \"wikidata-items\" } },\
{ \"remove\" : { \"index\" : \"wikidata-items-${OLD_DUMP_DATE}\", \"alias\" : \"wikidata-all\" } }\
]\
}"