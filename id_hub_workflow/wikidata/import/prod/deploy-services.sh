#!/usr/bin/env bash

WIKIDATA_PATH=/swissbib_index/nas/wikidata
export WIKIDATA_CONFIGS="$WIKIDATA_PATH/configs"

mkdir -p ${WIKIDATA_PATH}/dump
mkdir -p ${WIKIDATA_PATH}/meta && touch "$_/read_files.txt"

mkdir -p ${WIKIDATA_CONFIGS} && cp ./configs/events.csv ./configs/organisations.csv "$_"

kafka-topics create wikidata-all
kafka-topics create wikidata-filtered
kafka-topics create wikidata-categorized

docker stack deploy -c docker-compose.yml wikidata-import