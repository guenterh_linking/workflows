#!/usr/bin/env bash

ID_DATE=2022-05-22


curl -X POST 'http://192.168.1.53:9200/_aliases' -H 'Content-Type: application/json' -d"{\"actions\": \
[ \
{ \"add\" : { \"index\" : \"gnd-organisations-${ID_DATE}\", \"alias\" : \"gnd-organisations-next\" }}, \
{ \"add\" : { \"index\" : \"gnd-persons-${ID_DATE}\", \"alias\" : \"gnd-persons-next\" }}, \
{ \"add\" : { \"index\" : \"gnd-works-${ID_DATE}\", \"alias\" : \"gnd-works-next\" }}, \
{ \"add\" : { \"index\" : \"gnd-conferences-${ID_DATE}\", \"alias\" : \"gnd-conferences-next\" }}, \
{ \"add\" : { \"index\" : \"gnd-places-${ID_DATE}\", \"alias\" : \"gnd-places-next\" }}, \
{ \"add\" : { \"index\" : \"gnd-subjects-${ID_DATE}\", \"alias\" : \"gnd-subjects-next\" }}, \
{ \"add\" : { \"index\" : \"gnd-undiff-persons-${ID_DATE}\", \"alias\" : \"gnd-undiff-persons-next\"}}, \
{ \"add\" : { \"index\" : \"wikidata-persons-null\", \"alias\" : \"wikidata-persons-next\" }}, \
{ \"add\" : { \"index\" : \"wikidata-events-null\", \"alias\" : \"wikidata-events-next\" }}, \
{ \"add\" : { \"index\" : \"wikidata-items-null\", \"alias\" : \"wikidata-items-next\" }}, \
{ \"add\" : { \"index\" : \"wikidata-organisations-null\", \"alias\" : \"wikidata-organisations-next\" }} \
]}"

