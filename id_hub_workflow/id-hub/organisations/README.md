### Id Hub Process for Organisations

This process loads the owl:sameAs and @id fields from each data source.

It is important to run these in the correct order at the moment.

1. GND
2. VIAF
3. Wikidata
4. Dbpedia

Parallel execution or different orders can lead to different outcomes...

To create a new id-hub ensure that all data sets are fully loaded and have the *-next aliases
specified. Now update the [env.sh](../env.sh) ID_HUB_DATE variable to the current date.
Now deploy the services with [deploy-services.sh](./prod/deploy-services.sh).

Once the services are running and the Kafka topics are created, each of the run-{dataset}-workflow.sh files
can be used to process each dataset.

**NOTE**: The test configurations read from the production data sets.