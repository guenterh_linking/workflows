#!/usr/bin/env bash

source ../../env.sh

export ELASTIC_INDEX=id-hub-organisations-${ID_HUB_DATE}
export EXPORT_DATE=${ID_HUB_DATE}

curl -XPUT -H "Content-Type: application/json" --data "@./mappings/id-hub.json" "http://sb-ues5.swissbib.unibas.ch:8080/$ELASTIC_INDEX"

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"id-hub-organisations-${ID_HUB_DATE}\", \"alias\" : \"id-hub-next\" } }\
]\
}"

mkdir -p ${ID_HUB_CONFIGS_ORGANISATIONS}/gnd && cp ./configs/app.gnd.properties "$_/app.properties"
mkdir -p ${ID_HUB_CONFIGS_ORGANISATIONS}/dbpedia && cp ./configs/app.dbpedia.properties "$_/app.properties"
mkdir -p ${ID_HUB_CONFIGS_ORGANISATIONS}/viaf && cp ./configs/app.viaf.properties "$_/app.properties"
mkdir -p ${ID_HUB_CONFIGS_ORGANISATIONS}/wikidata && cp ./configs/app.wikidata.properties "$_/app.properties"
mkdir -p ${ID_HUB_CONFIGS_ORGANISATIONS}/rero && cp ./configs/app.rero.properties "$_/app.properties"