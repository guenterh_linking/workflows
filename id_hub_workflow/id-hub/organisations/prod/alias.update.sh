#!/usr/bin/env bash

source ../../env.sh

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"id-hub-organisations-${ID_HUB_DATE}\", \"alias\" : \"id-hub\" } },\
{ \"remove\" : { \"index\" : \"id-hub-organisations-${ID_HUB_OLD_DATE}\", \"alias\" : \"id-hub\" } },\
{ \"remove\" : { \"index\" : \"id-hub-organisations-${ID_HUB_DATE}\", \"alias\" : \"id-hub-next\" } }\
]\
}"