## ID Hub Workflow for Organisations

A workflow to create an ID Hub with the currently available data sets from VIAF, GND, WIKIDATA & DBPEDIA.

1. Ensure both services are running!
2. Start workflow with [start-workflow.sh](start-workflow.sh).
3. Update the aliases.