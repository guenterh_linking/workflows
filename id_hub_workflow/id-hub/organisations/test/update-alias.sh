#!/usr/bin/env bash

source ../../env.sh

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"test-id-hub-organisations-${ID_HUB_DATE}\", \"alias\" : \"test-id-hub\" } }\
]\
}"