#!/usr/bin/env bash

kafka-topics-test create id-hub-organisations-source
kafka-topics-test create id-hub-organisations-sink

docker stack deploy -c docker-compose.yml test-id-hub-organisations