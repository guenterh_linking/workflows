## Clustering Workflows

Workflows to create the ID clusters for the following types:

- [Persons](persons/README.md)
- [Organisations](organisations/README.md)

In general the workflows use owl:sameAs properties to construct the mapping
between the different data sources. At the moment no new links are created or
corrected. All links must exist in the source data sets.

The transformation service merges new links with the existing links and creates a cluster.
The general model can be found in the mappings files. The transformation process
will choose one of the uris as ID for the cluster.
It will prefer gnd > viaf.

Clusters without a GND or VIAF URI present will currently be ignored. The reason is, that we do not
have any data which would depend on this data.

It might make sense to prefer VIAF URIs as Cluster Id over GND URIs. VIAF URIs should always be present.

### Model

- `id`: Id of the cluster
- `created`: Date when this cluster was first created.
- `modified`: Date when this cluster was last modified.
- `identifiers.*`: An identifiers field for each provider (without the namespace part, keyword field for easy term matching).
- `resources.identifier`: Just the identifier
- `resources.uri`: The entire URI of the resource.
- `resources.provider`: The short form of the provider (see [Providers](../../shared-configurations/providers.json))

