#!/usr/bin/env bash

kafka-topics-test create id-hub-persons-source
kafka-topics-test create id-hub-persons-sink

docker stack deploy -c docker-compose.yml test-id-hub-persons