#!/usr/bin/env bash

source ../../env.sh

export ELASTIC_INDEX=test-id-hub-persons-${ID_HUB_DATE}
export EXPORT_DATE=${ID_HUB_DATE}

curl -XPUT -H "Content-Type: application/json" --data "@./mappings/id-hub.json" "http://sb-ues5.swissbib.unibas.ch:8080/$ELASTIC_INDEX"

mkdir -p ${ID_HUB_CONFIGS_PERSONS}/gnd && cp ./configs/app.gnd.properties "$_/app.properties"
mkdir -p ${ID_HUB_CONFIGS_PERSONS}/dbpedia && cp ./configs/app.dbpedia.properties "$_/app.properties"
mkdir -p ${ID_HUB_CONFIGS_PERSONS}/viaf && cp ./configs/app.viaf.properties "$_/app.properties"
mkdir -p ${ID_HUB_CONFIGS_PERSONS}/wikidata && cp ./configs/app.wikidata.properties "$_/app.properties"