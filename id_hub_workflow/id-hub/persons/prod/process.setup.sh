#!/usr/bin/env bash

source ../../env.sh

export ELASTIC_INDEX=id-hub-persons-${ID_HUB_DATE}
export EXPORT_DATE=${ID_HUB_DATE}

curl -XPUT -H "Content-Type: application/json" --data "@./mappings/id-hub.json" "http://192.168.1.53:9200/$ELASTIC_INDEX"

curl -X POST "http://192.168.1.53:9200/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"id-hub-persons-${ID_HUB_DATE}\", \"alias\" : \"id-hub-next\" } }\
]\
}"

#mkdir -p ${ID_HUB_CONFIGS_PERSONS}/gnd && cp ./configs/app.gnd.properties "$_/app.properties"
#mkdir -p ${ID_HUB_CONFIGS_PERSONS}/dbpedia && cp ./configs/app.dbpedia.properties "$_/app.properties"
#mkdir -p ${ID_HUB_CONFIGS_PERSONS}/viaf && cp ./configs/app.viaf.properties "$_/app.properties"
#mkdir -p ${ID_HUB_CONFIGS_PERSONS}/wikidata && cp ./configs/app.wikidata.properties "$_/app.properties"
#mkdir -p ${ID_HUB_CONFIGS_PERSONS}/rero && cp ./configs/app.rero.properties "$_/app.properties"