#!/usr/bin/env bash


ID_HUB_DATE=2022-05-25

export ELASTIC_INDEX=gnd-persons-next-${ID_HUB_DATE}

curl -XPUT -H "Content-Type: application/json" --data "@./mappings/id-hub.json" "http://192.168.1.53:9200/$ELASTIC_INDEX"

