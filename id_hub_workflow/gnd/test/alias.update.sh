#!/usr/bin/env bash

NEW_DUMP_DATE=2019-02-18

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"test-gnd-organisations-${NEW_DUMP_DATE}\", \"alias\" : \"test-gnd-sb-organisations\" } },\
{ \"add\" : { \"index\" : \"test-gnd-conferences-${NEW_DUMP_DATE}\", \"alias\" : \"test-gnd-sb-organisations\" } },\
{ \"add\" : { \"index\" : \"test-gnd-conferences-${NEW_DUMP_DATE}\", \"alias\" : \"test-gnd-conferences-next\" } },\
{ \"add\" : { \"index\" : \"test-gnd-persons-${NEW_DUMP_DATE}\", \"alias\" : \"test-gnd-persons\" } },\
{ \"add\" : { \"index\" : \"test-gnd-undiff-persons-${NEW_DUMP_DATE}\", \"alias\" : \"test-gnd-all\" } },\
{ \"add\" : { \"index\" : \"test-gnd-works-${NEW_DUMP_DATE}\", \"alias\" : \"test-gnd-all\" } },\
{ \"add\" : { \"index\" : \"test-gnd-places-${NEW_DUMP_DATE}\", \"alias\" : \"test-gnd-all\" } },\
{ \"add\" : { \"index\" : \"test-gnd-subjects-${NEW_DUMP_DATE}\", \"alias\" : \"test-gnd-all\" } },\
{ \"add\" : { \"index\" : \"test-gnd-organisations-${NEW_DUMP_DATE}\", \"alias\" : \"test-gnd-all\" } },\
{ \"add\" : { \"index\" : \"test-gnd-conferences-${NEW_DUMP_DATE}\", \"alias\" : \"test-gnd-all\" } },\
{ \"add\" : { \"index\" : \"test-gnd-persons-${NEW_DUMP_DATE}\", \"alias\" : \"test-gnd-all\" } }\
]\
}"\
