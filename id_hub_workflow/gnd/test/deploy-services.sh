#!/usr/bin/env bash

mkdir -p /swissbib_index/nas/test/gnd/dump
mkdir -p /swissbib_index/nas/test/gnd/meta && touch "$_/read_files.txt"

kafka-topics-test create gnd-all
kafka-topics-test create gnd-categorized

docker stack deploy -c docker-compose.yml test-gnd-import