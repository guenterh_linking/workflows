#!/usr/bin/env bash

mkdir -p /swissbib_index/nas/test/gnd/downloader && cp ./configs/download/* "$_/"

docker-compose -f docker-compose.download.yml up -d