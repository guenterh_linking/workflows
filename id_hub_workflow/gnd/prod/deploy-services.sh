#!/usr/bin/env bash

mkdir -p /swissbib_index/nas/gnd/dump
mkdir -p /swissbib_index/nas/gnd/meta && touch "$_/read_files.txt"

kafka-topics create gnd-all
kafka-topics create gnd-categorized

docker stack deploy -c docker-compose.yml gnd-import