#!/usr/bin/env bash

OLD_DUMP_DATE=2020-03-16
NEW_DUMP_DATE=2020-04-08

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"gnd-organisations-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"gnd-organisations-${OLD_DUMP_DATE}\", \"alias\" : \"gnd-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"gnd-organisations-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-sb-organisations-next\" } },\
{ \"add\" : { \"index\" : \"gnd-conferences-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"gnd-conferences-${OLD_DUMP_DATE}\", \"alias\" : \"gnd-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"gnd-conferences-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-sb-organisations-next\" } },\
{ \"add\" : { \"index\" : \"gnd-persons-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-persons\" } },\
{ \"remove\" : { \"index\" : \"gnd-persons-${OLD_DUMP_DATE}\", \"alias\" : \"gnd-persons\" } },\
{ \"remove\" : { \"index\" : \"gnd-persons-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-persons-next\" } },\
{ \"add\" : { \"index\" : \"gnd-undiff-persons-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"remove\" : { \"index\" : \"gnd-undiff-persons-${OLD_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"add\" : { \"index\" : \"gnd-works-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"remove\" : { \"index\" : \"gnd-works-${OLD_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"add\" : { \"index\" : \"gnd-places-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"remove\" : { \"index\" : \"gnd-places-${OLD_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"add\" : { \"index\" : \"gnd-subjects-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"remove\" : { \"index\" : \"gnd-subjects-${OLD_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"add\" : { \"index\" : \"gnd-organisations-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"remove\" : { \"index\" : \"gnd-organisations-${OLD_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"add\" : { \"index\" : \"gnd-conferences-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"remove\" : { \"index\" : \"gnd-conferences-${OLD_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"add\" : { \"index\" : \"gnd-persons-${NEW_DUMP_DATE}\", \"alias\" : \"gnd-all\" } },\
{ \"remove\" : { \"index\" : \"gnd-persons-${OLD_DUMP_DATE}\", \"alias\" : \"gnd-all\" } }\
]\
}"\
