#!/usr/bin/env bash

source ../../env.sh

mkdir -p ${PRODUCER_META} && touch "$_/read_files.txt"
mkdir -p ${VIAF_IMPORT_LINKS_DIR}

kafka-topics create viaf-links-all
kafka-topics create viaf-links-categorized

export VIAF_IMPORT_LINKS_DIR=${VIAF_IMPORT_LINKS_DIR}
export PRODUCER_META=${PRODUCER_META}

docker stack deploy -c docker-compose.yml viaf-links-import