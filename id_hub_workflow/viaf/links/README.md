## VIAF Links Import Workflow

This workflow takes the viaf justlinks dataset provided on http://viaf.org/viaf/data/. This dataset is a simple
text file with each line representing a link between a viaf ID and a provider ID. More links can be found in this
data set than in the schema:sameAs statements attached to the viaf cluster. 

Currently RERO and Wikipedia Links are indexed in their respective indices.

1. Adjust date in [env.sh](../env.sh) to latest dump from the viaf release page.
2. Ensure that all services are running with [deploy-services.sh](prod/deploy-services.sh)
3. Run workflow with [start-workflow.sh](prod/start-workflow.sh)
4. Update aliases once all data records are indexed.


