#!/usr/bin/env bash

source ../../env.sh

mkdir -p ${TEST_PRODUCER_META} && touch "$_/read_files.txt"
mkdir -p ${TEST_VIAF_IMPORT_LINKS_DIR}

kafka-topics-test create viaf-links-all
kafka-topics-test create viaf-links-categorized

export VIAF_IMPORT_LINKS_DIR=${TEST_VIAF_IMPORT_LINKS_DIR}
export PRODUCER_META=${TEST_PRODUCER_META}

docker stack deploy -c docker-compose.yml viaf-links-import