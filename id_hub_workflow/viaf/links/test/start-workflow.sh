#!/usr/bin/env bash

source ../../env.sh

mkdir -p ${TEST_VIAF_IMPORT_DOWNLOAD_DIR}/tmp
mkdir -p ${TEST_VIAF_IMPORT_LINKS_DIR}

ELASTIC_HOST=http://sb-ues5.swissbib.unibas.ch:8080

curl -XPUT -H "Content-Type: application/json" --data "@./mapping/rero.mapping.json" "$ELASTIC_HOST/test-rero-viaf-links-${VIAF_DUMP_DATE}"
curl -XPUT -H "Content-Type: application/json" --data "@./mapping/wikipedia.mapping.json" "$ELASTIC_HOST/test-wikipedia-viaf-links-${VIAF_DUMP_DATE}"

curl -X POST "$ELASTIC_HOST/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":[\
{ \"add\" : { \"index\" : \"test-rero-viaf-links-${VIAF_DUMP_DATE}\", \"alias\" : \"test-rero-viaf-links-next\" } },\
{ \"add\" : { \"index\" : \"test-wikipedia-viaf-links-${VIAF_DUMP_DATE}\", \"alias\" : \"test-wikipedia-viaf-links-next\" } }\
]}"

FILE="$TEST_VIAF_IMPORT_DOWNLOAD_DIR/tmp/viaf-$VIAF_DUMP_DATE-links.txt.gz"

if [[ -f "$FILE" ]]; then
    echo "latest file has already been downloaded!"
else
    echo "downloading latest links file $FILE."
    wget -q -O "$FILE" http://viaf.org/viaf/data/viaf-${VIAF_DUMP_DATE_COMPACT}-links.txt.gz
    mv "$TEST_VIAF_IMPORT_DOWNLOAD_DIR/tmp/viaf-$VIAF_DUMP_DATE-links.txt.gz" "$TEST_VIAF_IMPORT_LINKS_DIR/viaf-$VIAF_DUMP_DATE-links.txt.gz"
fi

