### VIAF Import

This process is used to import the latest viaf data & links in elasticsearch.
The data files are publish on [VIAF Dataset](http://viaf.org/viaf/data/).

#### Core Workflow

Import the latest data dump. Follow the instructions [here](import/README.md).

#### Secondary Links

This workflow creates indices of relevant "same as" relations which are not included in the rdf dump!

Follow the instructions [here](links/README.md).