#!/usr/bin/env bash

source ../../env.sh

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"remove\" : { \"index\" : \"viaf-organisations-${VIAF_OLD_DUMP_DATE}\", \"alias\" : \"viaf-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"viaf-events-${VIAF_OLD_DUMP_DATE}\", \"alias\" : \"viaf-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"viaf-persons-${VIAF_OLD_DUMP_DATE}\", \"alias\" : \"viaf-persons\" } },\
{ \"remove\" : { \"index\" : \"viaf-persons-${VIAF_OLD_DUMP_DATE}\", \"alias\" : \"viaf-all\" } },\
{ \"remove\" : { \"index\" : \"viaf-events-${VIAF_OLD_DUMP_DATE}\", \"alias\" : \"viaf-all\" } },\
{ \"remove\" : { \"index\" : \"viaf-organisations-${VIAF_OLD_DUMP_DATE}\", \"alias\" : \"viaf-all\" } },\
{ \"remove\" : { \"index\" : \"viaf-places-${VIAF_OLD_DUMP_DATE}\", \"alias\" : \"viaf-all\" } },\
{ \"remove\" : { \"index\" : \"viaf-creative-works-${VIAF_OLD_DUMP_DATE}\", \"alias\" : \"viaf-all\" } }\
]\
}"\

