#!/usr/bin/env bash

source ../../env.sh

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"viaf-organisations-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"viaf-organisations-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-sb-organisations-next\" } },\
{ \"add\" : { \"index\" : \"viaf-events-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-sb-organisations\" } },\
{ \"remove\" : { \"index\" : \"viaf-events-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-sb-organisations-next\" } },\
{ \"add\" : { \"index\" : \"viaf-persons-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-persons\" } },\
{ \"remove\" : { \"index\" : \"viaf-persons-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-persons-next\" } },\
{ \"add\" : { \"index\" : \"viaf-persons-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-all\" } },\
{ \"add\" : { \"index\" : \"viaf-events-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-all\" } },\
{ \"add\" : { \"index\" : \"viaf-organisations-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-all\" } },\
{ \"add\" : { \"index\" : \"viaf-places-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-all\" } },\
{ \"add\" : { \"index\" : \"viaf-creative-works-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-all\" } }\
]\
}"\

