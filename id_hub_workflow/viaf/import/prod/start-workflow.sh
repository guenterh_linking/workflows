#!/usr/bin/env bash

source ../../env.sh

kafka-topics create viaf-all
kafka-topics create viaf-categorized

mkdir -p ${VIAF_IMPORT_TRIPLES_DIR}

FILE="$VIAF_IMPORT_TRIPLES_DIR/viaf-$VIAF_DUMP_DATE_COMPACT-clusters-rdf.nt.gz"

if [[ -f "$FILE" ]]; then
    echo "latest file has already been downloaded!"
else
  echo "downloading latest viaf data dump!"
  wget -q -O "$FILE" "http://viaf.org/viaf/data/viaf-$VIAF_DUMP_DATE_COMPACT-clusters-rdf.nt.gz"
fi

ELASTIC_HOST="http://sb-ues5.swissbib.unibas.ch:8080"

curl -XPUT -H "Content-Type: application/json" --data "@./mappings/organisations.json" "$ELASTIC_HOST/viaf-organisations-${VIAF_DUMP_DATE}"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/places.json" "$ELASTIC_HOST/viaf-places-${VIAF_DUMP_DATE}"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/events.json" "$ELASTIC_HOST/viaf-events-${VIAF_DUMP_DATE}"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/persons.json" "$ELASTIC_HOST/viaf-persons-${VIAF_DUMP_DATE}"
curl -XPUT -H "Content-Type: application/json" --data "@./mappings/creative-works.json" "$ELASTIC_HOST/viaf-creative-works-${VIAF_DUMP_DATE}"


curl -X POST "$ELASTIC_HOST/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":[\
{ \"add\" : { \"index\" : \"viaf-organisations-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-sb-organisations-next\" } },\
{ \"add\" : { \"index\" : \"viaf-events-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-sb-organisations-next\" } },\
{ \"add\" : { \"index\" : \"viaf-persons-${VIAF_DUMP_DATE}\", \"alias\" : \"viaf-persons-next\" } }\
]}"


sed "s/__VIAF_DUMP_DATE__/$VIAF_DUMP_DATE/g;s/__VIAF_DUMP_DATE_COMPACT__/$VIAF_DUMP_DATE_COMPACT/g" ./configs/template.properties > app.properties

mkdir -p ${VIAF_IMPORT_CONFIGS_DIR}/sort && mv app.properties "$_/"

ssh "swissbib@sb-ust1.swissbib.unibas.ch" "/swissbib_index/apps/flink/bin/flink run /swissbib_index/apps/viaf-grouped-ntriples-assembly-1.0.1.jar --config $VIAF_IMPORT_CONFIGS_DIR/sort/app.properties" &
