<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Copyright (C) 2019  Project swissbib <https://swissbib.org>
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as
  ~ published by the Free Software Foundation, either version 3 of the
  ~ License, or (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
  -->

<metamorph xmlns="http://www.culturegraph.org/metamorph" version="1">

    <macros>
        <macro name="language-tag">
            <entity name="$[abbrev]" flushWith="$[predicate]" reset="true">
                <combine name="${lang}" value="${value}" flushWith="$[predicate]">
                    <data name="value" source="$[predicate]">
                        <replace pattern='##@.+' with=''/>
                    </data>
                    <data name="lang" source="$[predicate]">
                        <regexp match='##@([^ ]+)' format="${1}"/>
                    </data>
                </combine>
            </entity>
        </macro>
        <macro name="filter-language-tag">
            <combine name="$[tempName]" value="${value}" flushWith="$[predicate]" reset="true">
                <if>
                    <data source="$[predicate]">
                        <split delimiter="##"/>
                        <occurrence only="moreThan 1"/>
                        <whitelist>
                            <entry name="@en"/>
                            <entry name="@en-GB"/>
                            <entry name="@en-US"/>
                            <entry name="@de"/>
                            <entry name="@de-AT"/>
                            <entry name="@de-CH"/>
                            <entry name="@de-DE"/>
                            <entry name="@it"/>
                            <entry name="@it-IT"/>
                            <entry name="@it-CH"/>
                            <entry name="@it-VA"/>
                            <entry name="@fr"/>
                            <entry name="@fr-FR"/>
                            <entry name="@fr-CH"/>
                        </whitelist>
                    </data>
                </if>
                <data source="$[predicate]" name="value"/>
            </combine>
        </macro>
        <macro name="remove-datatype">
            <data source="$[predicate]" name="$[abbrev]">
                <split delimiter="##"/>
                <occurrence only="lessThan 2"/>
            </data>
        </macro>
    </macros>

    <rules>
        <!-- identifier should only be present once. Otherwise the context is added multiple times.-->
        <data source="http://purl.org/dc/terms/identifier" name="\@context">
            <constant value="https://resources.swissbib.ch/viaf/context.jsonld"/>
        </data>

        <data source="_id" name="\@id"/>

        <!-- RDF -->
        <data source="http://www.w3.org/1999/02/22-rdf-syntax-ns#type" name="\@type"/>

        <!-- DBPedia -->
        <data source="http://dbpedia.org/ontology/notableWork" name="dbo:notableWork"/>

        <!-- Dublin Core -->
        <data source="http://purl.org/dc/terms/identifier" name="dct:identifier"/>

        <!-- RDF Schema -->
        <data source="http://www.w3.org/2000/01/rdf-schema#label" name="rdfs:label"/>

        <!-- schema.org -->
        <data source="http://schema.org/alternateName" name="schema:alternateName"/>
        <data source="http://schema.org/location" name="schema:location"/>
        <!-- schema:description -->
        <call-macro name="filter-language-tag" predicate="http://schema.org/description" tempName="@descriptionFiltered"/>
        <call-macro name="language-tag" predicate="@descriptionFiltered" abbrev="schema:description"/>
        <!-- schema:name -->
        <call-macro name="filter-language-tag" predicate="http://schema.org/name" tempName="@nameFiltered"/>
        <call-macro name="language-tag" predicate="@nameFiltered" abbrev="schema:name"/>

        <!-- renamed as in our data we generally use owl:sameAs. They are semantically equivalent. -->
        <data source="http://schema.org/sameAs" name="owl:sameAs"/>

        <!-- skos:prefLabel -->
        <call-macro name="filter-language-tag" predicate="http://www.w3.org/2004/02/skos/core#prefLabel" tempName="@prefLabelFiltered"/>
        <call-macro name="language-tag" predicate="@@prefLabelFiltered" abbrev="skos:prefLabel"/>
    </rules>

</metamorph>