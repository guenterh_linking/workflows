### Core VIAF Data Ingest Process

The workflow needs to be started when a new dump is available [here](http://viaf.org/viaf/data/).

1. Change the variable OLD_VIAF_DUMP_DATE in [aliases.update.sh](aliases.update.sh) to the value found in 
the variable VIAF_DUMP_DATE from [env.sh](../../env.sh) and adjust the date of the 
dump in [env.sh](../../env.sh) (both variables, same format... the newest date can 
be found [here](http://viaf.org/viaf/data/)). 

2. Ensure the services are running with [deploy-services.sh](deploy-services.sh).

3. Start workflow with [start-workflow.sh](start-workflow.sh). This takes a long time (4-5 hours).

4. Use [aliases.update.sh](aliases.update.sh) to update the aliases once the new dataset is index and delete the
old indices.