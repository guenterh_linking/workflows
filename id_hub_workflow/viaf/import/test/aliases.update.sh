#!/usr/bin/env bash

source ../../env.sh

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"test-viaf-organisations-${VIAF_DUMP_DATE}\", \"alias\" : \"test-viaf-sb-organisations\" } },\
{ \"add\" : { \"index\" : \"test-viaf-events-${VIAF_DUMP_DATE}\", \"alias\" : \"test-viaf-sb-organisations\" } },\
{ \"add\" : { \"index\" : \"test-viaf-persons-${VIAF_DUMP_DATE}\", \"alias\" : \"test-viaf-persons\" } },\
{ \"add\" : { \"index\" : \"test-viaf-persons-${VIAF_DUMP_DATE}\", \"alias\" : \"test-viaf-all\" } },\
{ \"add\" : { \"index\" : \"test-viaf-events-${VIAF_DUMP_DATE}\", \"alias\" : \"test-viaf-all\" } },\
{ \"add\" : { \"index\" : \"test-viaf-organisations-${VIAF_DUMP_DATE}\", \"alias\" : \"test-viaf-all\" } },\
{ \"add\" : { \"index\" : \"test-viaf-places-${VIAF_DUMP_DATE}\", \"alias\" : \"test-viaf-all\" } },\
{ \"add\" : { \"index\" : \"test-viaf-creative-works-${VIAF_DUMP_DATE}\", \"alias\" : \"test-viaf-all\" } }\
]\
}"\

