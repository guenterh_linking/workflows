#!/usr/bin/env bash

source ../../env.sh

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"dbpedia-uris-${DBPEDIA_GLOBAL_URIS_DUMP_DATE}\", \"alias\" : \"dbpedia-uris\" } }\
]\
}"