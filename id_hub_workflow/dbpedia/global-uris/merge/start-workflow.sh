#!/usr/bin/env bash

source ../../env.sh

INDEX_DATE=${DBPEDIA_DUMP_DATE}
export DBPEDIA_CURRENT_EVENTS_INDEX="dbpedia-events-$INDEX_DATE"
export DBPEDIA_CURRENT_ORGANISATIONS_INDEX="dbpedia-organisations-$INDEX_DATE"
export DBPEDIA_CURRENT_PERSONS_INDEX="dbpedia-persons-$INDEX_DATE"

export MERGE_CONFIGS_PATH="$URIS_CONFIG_PATH/merge"

mkdir -p ${MERGE_CONFIGS_PATH}/producer/persons && cp ./configs/app.persons.properties "$_/app.properties"
mkdir -p ${MERGE_CONFIGS_PATH}/producer/organisations && cp ./configs/app.organisations.properties "$_/app.properties"
mkdir -p ${MERGE_CONFIGS_PATH}/producer/events && cp ./configs/app.events.properties "$_/app.properties"

docker-compose up -d