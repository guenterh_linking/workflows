#!/usr/bin/env bash


export DBPEDIA_ELASTIC_INDEX=dbpedia-analyzed-2019-09-24

mkdir -p /swissbib_index/nas/configurations/dbpedia/analyzer && cp ./configs/* "$_"

curl -XPUT -H "Content-Type: application/json" --data "@./mappings/mapping.json" "http://sb-ues5.swissbib.unibas.ch:8080/${DBPEDIA_ELASTIC_INDEX}"

kafka-topics create dbpedia-analyzed

docker-compose -H "sb-uwf3.swissbib.unibas.ch:2375" pull
docker-compose -H "sb-uwf3.swissbib.unibas.ch:2375" up -d