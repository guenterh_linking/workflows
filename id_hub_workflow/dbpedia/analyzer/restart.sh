#!/usr/bin/env bash

docker-compose -H "sb-uwf3.swissbib.unibas.ch:2375" stop
docker-compose -H "sb-uwf3.swissbib.unibas.ch:2375" start