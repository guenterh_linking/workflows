#!/usr/bin/env bash

source ../env.sh

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"dbpedia-organisations-${DBPEDIA_DUMP_DATE}\", \"alias\" : \"dbpedia-organisations-next\" } },\
{ \"add\" : { \"index\" : \"dbpedia-events-${DBPEDIA_DUMP_DATE}\", \"alias\" : \"dbpedia-organisations-next\" } },\
{ \"add\" : { \"index\" : \"dbpedia-persons-${DBPEDIA_DUMP_DATE}\", \"alias\" : \"dbpedia-persons-next\" } }\
]\
}"\

