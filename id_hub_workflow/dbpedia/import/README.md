## DBpedia Import Workflow

**IT IS CURRENTLY NOT NECESSARY TO RUN THIS PROCESS AS THERE ARE NO UPDATES AVAILABLE ON THE DATABUS!**

To import dbpedia run this workflow. 

1. Ensure that all services are running with [deploy-services.sh](deploy-services.sh).
2. Update the dump date in the [env.sh](../env.sh) file to the latest dbpedia release.
3. Start the workflow execution with [start-workflow.sh](start-workflow.sh) 
(wait for the script to complete or run it as background job)
4. Update the aliases to the latest indices and remove the old indices once the dataset is indexed.


