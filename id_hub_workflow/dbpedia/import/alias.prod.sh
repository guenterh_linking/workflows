#!/usr/bin/env bash

source ../env.sh

curl -X POST "http://sb-ues5.swissbib.unibas.ch:8080/_aliases" -H 'Content-Type: application/json' -d"{\"actions\":\
[\
{ \"add\" : { \"index\" : \"dbpedia-organisations-${DBPEDIA_DUMP_DATE}\", \"alias\" : \"dbpedia-all\" } },\
{ \"add\" : { \"index\" : \"dbpedia-organisations-${DBPEDIA_DUMP_DATE}\", \"alias\" : \"dbpedia-organisations\" } },\
{ \"add\" : { \"index\" : \"dbpedia-others-${DBPEDIA_DUMP_DATE}\", \"alias\" : \"dbpedia-all\" } },\
{ \"add\" : { \"index\" : \"dbpedia-events-${DBPEDIA_DUMP_DATE}\", \"alias\" : \"dbpedia-all\" } },\
{ \"add\" : { \"index\" : \"dbpedia-events-${DBPEDIA_DUMP_DATE}\", \"alias\" : \"dbpedia-organisations\" } },\
{ \"add\" : { \"index\" : \"dbpedia-persons-${DBPEDIA_DUMP_DATE}\", \"alias\" : \"dbpedia-all\" } },\
{ \"add\" : { \"index\" : \"dbpedia-persons-${DBPEDIA_DUMP_DATE}\", \"alias\" : \"dbpedia-persons\" } }\
]\
}"\

