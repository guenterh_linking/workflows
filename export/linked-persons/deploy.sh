#!/usr/bin/env bash


mkdir -p /swissbib_index/nas/configurations/export/linked-persons/producer && cp ./configs/app.producer.properties "$_/app.properties"
mkdir -p /swissbib_index/nas/configurations/export/linked-persons/consumer && cp ./configs/app.consumer.properties "$_/app.properties"
mkdir -p /swissbib_index/nas/export/linked-persons

docker-compose -H "sb-uwf2.swissbib.unibas.ch:2375" pull
docker-compose -H "sb-uwf2.swissbib.unibas.ch:2375" up -d