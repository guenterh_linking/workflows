## Shared Configuration Files

A collection of configuration files which are shared in at least two containers.

### providers.json

Used by the services 

- id-hub-persons_merger
- id-hub-organisations_merger
- sb-linker
- sb-resource-linker (baseline)
- sb-enricher

Need to update these docker-compose files and rerun the service deployment each time the
configurations change.